import Vue from 'vue';
import HighchartsVue from 'highcharts-vue';
import App from './App.vue';

import './scss/style.scss';

Vue.use(HighchartsVue);

new Vue({
  el: '#app',
  render: h => h(App),
});
